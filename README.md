Wintersmith Gallery Plugin
===

This plugin lets you publish directories with images in them. It then creates galleries index page with links to 
individual galleries and those individual galleries as one page for each directory containing accepted images.

# Usage

Include the plugin in _config.json_:

    "plugins": [
        "@zladuric/wintersmith-gallery"
    ],
    

Create the templates: one for gallery index and another for individual galleries.
[Examples](examples/galleries-index.pug) [here](examples/gallery.pug).

That's it. if you drop a directory full of images under `contents/galleries` and `wintersmith build` the site, you'll have
a bunch of new pages with your images.

## Configuration

Optionally configure it in _config.json_:

    {
        "template": "gallery.pug",
        "indexTemplate": "galleries-index.pug", 
        "galleries": "galleries", 
        "validImageTypes": ["png", "jpg", "jpeg"], // which types of templates are valid for inclusion
        "galleriesIndex": "galleries/index.html",
        "galleryPageIndex": "==GALLERY==/index.html"
        "perPage": 10,
        "galleriesRootMandatory": true
    }
    
Options (`option name` [default]: description):    

- `template` [_galery.pug_]: template for individual gallery, [example](examples/gallery.pug)
- `indexTemplate` [_galleries-index.pug_]:  template for index page with links to all galleries, [example](examples/galleries-index.pug) 
- `galleries` [_galleries_]: where under `contents` to look for galleries directories 
- `validImageTypes` [_["png", "jpg", "jpeg"]_]: which types of templates are valid for inclusion. _Note: only checked by file extension._
- `galleriesIndex` [_galleries/index.html"_]: _filename_ for the index page 
- `galleryPageIndex` [_==GALLERY==/index.html_]: _filename_ for the individual gallery. **Note: _MUST_ include the string ==GALLERY==.**
- `galleriesRootMandatory` [_true_]: fail the build if there's no root directory for galleries. 
