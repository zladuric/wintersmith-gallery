module.exports = function galleriesIndexPageBuilder(options, Page) {
  return class GalleriesIndexPage extends Page {
    constructor(galleries) {
      super();
      this.galleries = galleries;
    }

    // eslint-disable-next-line class-methods-use-this
    getFilename() {
      return options.galleriesIndex;
    }

    // eslint-disable-next-line class-methods-use-this
    getView() {
      return function view(env, locals, contents, templates, callback) {
        const template = templates[options.indexTemplate];
        if (template === null) {
          return callback(new Error(`unknown categories home template '${options.template}'`));
        }
        // setup the template context
        const ctx = {
          galleries: this.galleries,
        };

        // extend the template context with the environment locals
        env.utils.extend(ctx, locals);

        // finally render the template
        return template.render(ctx, callback);
      };
    }
  };
};
