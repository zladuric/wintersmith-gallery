const { sep } = require('path');
/* eslint-disable no-use-before-define */
/**
 * Returns the { galleryName: string, gallery: contentTree, images: StaticFile[] } in the given
 * ContentRoot, or directory where all the directory galleries are.
 *
 * @param  {ContentRoot} contentRoot root directory where gallery dirs are dropped in
 * @param  {Object} options     Gallery plugin options (// see constants.js#defaultOptions)
 * @return {{ name: string, gallery: ContentTree, images: StaticFile[] }}     list of galleries
 */
function getGalleries(contentRoot, options) {
  if (!contentRoot[options.galleries]) {
    throw new Error(`Galleries directory "${options.galleries}" not found.`);
  }
  const entries = contentRoot[options.galleries]._.directories;
  return entries
    .map((entry) => ({
      name: getGalleryName(entry),
      gallery: entry,
      images: getImagesInGallery(entry, options),
    }))
    .filter((gallery) => gallery.images.length > 0);
}

/**
 * Returns the image-type files (filtered by file extension)
 * @private
 */
function getImagesInGallery(gallery, options) {
  return filterFilenamesByExtensions(gallery._.files, options.validImageTypes);
}

/**
 * grunt-work here.
 * @private
 */
function filterFilenamesByExtensions(files, validExtensions = []) {
  const extensions = validExtensions.map((ext) => ext.toLowerCase());
  return files.filter((file) => {
    const fileExtension = file.getFilename().match(/\.([^.]+)$/);
    if (!fileExtension) {
      return false;
    }
    return extensions.includes(fileExtension[1].toLowerCase());
  });
}

/**
 * capitaliue file names
 * @private
 */
function getGalleryName(entry) {
  // get the part after the last "/" and split by whitespace.
  const words = entry.filename
    .substring(entry.filename.lastIndexOf(sep) + 1)
    .split(/\s/g);
  // capitalize dem words and join it all back with whitespace.
  return words
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
}
module.exports = {
  getGalleries,
};
