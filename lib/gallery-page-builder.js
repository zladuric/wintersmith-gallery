const { ROOTDIR_NAME_PLACEHOLDER } = require('./constants');

module.exports = function galleryPageBuilder(options, Page) {
  return class GalleryPage extends Page {
    /**
     * A page has a number, we get articles, and we also gets page count and category
     */
    constructor(gallery) {
      super();
      this.galleryName = gallery.name;
      this.gallery = gallery.gallery;
      this.images = gallery.images;
    }

    getFilename() {
      return options.galleryPageIndex
        .replace(ROOTDIR_NAME_PLACEHOLDER, this.gallery.filename);
    }

    // eslint-disable-next-line class-methods-use-this
    getView() {
      return function view(env, locals, contents, templates, callback) {
        // simple view to pass articles and pageNum to the paginator template
        // note that this function returns a function

        // get the pagination template
        const template = templates[options.template];
        if (template === null) {
          return callback(new Error(`unknown categories template '${options.template}'`));
        }

        // setup the template context
        const ctx = {
          images: this.images,
          gallery: this.gallery,
          galleryName: this.galleryName,
        };

        // extend the template context with the environment locals
        env.utils.extend(ctx, locals);

        // finally render the template
        return template.render(ctx, callback);
      };
    }
  };
};
