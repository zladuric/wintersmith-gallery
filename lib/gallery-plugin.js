const { galleryDefaults } = require('./constants');
const { getGalleries } = require('./util');
const galleryPageBuilder = require('./gallery-page-builder');
const galleriesIndexPageBuilder = require('./galleries-index-page-builder');

/**
 * This is a Gallery wintersmith plugin.
 * Give it a directory called "gallery", and each directory within it should contain photos.
 * We'll build a small picture gallery out of each, and a small galleries home page..
 */
module.exports = function galleryPlugin(env, callback) {
  // assign defaults any option not set in the config file
  const options = {
    ...galleryDefaults,
    ...env.config.gallery,
  };

  // register a generator for 'gallery' plugin.
  env.registerGenerator('gallery', (contents, cb) => {
    const galleries = getGalleries(contents, options);
    // create the object that will be merged with the content tree (contents)
    const galleryData = {
      pages: {},
    };
    const GalleriesIndexPage = galleriesIndexPageBuilder(options, env.plugins.Page);
    const GalleryPage = galleryPageBuilder(options, env.plugins.Page);
    // add index
    galleryData.pages['index.page'] = new GalleriesIndexPage(galleries);
    // add each gallery
    for (const gallery of galleries) {
      galleryData.pages[`galleries.${gallery.name}.page`] = new GalleryPage(gallery);
    }
    // cb with the generated contents
    return cb(null, galleryData);
  });

  // add the article helper to the environment so we can use it later
  // eslint-disable-next-line no-param-reassign
  env.helpers.getGalleries = getGalleries;

  // tell the plugin manager we are done
  return callback();
};
